import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'list',
    loadChildren: './list/list.module#ListPageModule'
  },
  {
    path: 'business',
    loadChildren: './business/business.module#BusinessPageModule'
  },
  { path: 'entertainment', loadChildren: './entertainment/entertainment.module#EntertainmentPageModule' },
  { path: 'health', loadChildren: './health/health.module#HealthPageModule' },
  { path: 'science', loadChildren: './science/science.module#SciencePageModule' },
  { path: 'sports', loadChildren: './sports/sports.module#SportsPageModule' },
  { path: 'technology', loadChildren: './technology/technology.module#TechnologyPageModule' },
  { path: 'newsdetail', loadChildren: './newsdetail/newsdetail.module#NewsdetailPageModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
